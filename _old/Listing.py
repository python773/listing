import os
import datetime
from win32api import GetFileVersionInfo, LOWORD, HIWORD
import sys

from collections import Counter

#function to get version of file
def get_version_number(filename):
    try:
        info = GetFileVersionInfo (filename, "\\")
        ms = info['FileVersionMS']
        ls = info['FileVersionLS']
        return HIWORD (ms), LOWORD (ms), HIWORD (ls), LOWORD (ls)
    except:
        return "; "


path =os.getcwd()
print(path)
lists=[]#files list
exts=[] #extensions list
exts_unique=[] #unique extensions list    
#get files from path
for root, dirs, files in os.walk(path):
   for name in files:
       lists.append(root+"\\"+name) #file list
       filename, file_extension = os.path.splitext(name)
       exts.append(file_extension)
#save listing to file
with open(path+"\\listing.txt","w") as f: 
   f.write("File with listing from folder: " + path + "\n" +"\n"+ "name ; path ; size [B] ; creation_time ; modification_time ; last_use_time ; version \n")  
   i=0
   for file in lists:
      f.write("\n {} ; {} ; {} ; {} ; {} ; {} ; {}".format(str(file[file.rfind("\\",)+1:]),file,\
      str(os.path.getsize(file)),str(datetime.datetime.fromtimestamp(os.path.getctime(file))),\
      str(datetime.datetime.fromtimestamp(os.path.getmtime(file))), str(datetime.datetime.fromtimestamp(os.path.getatime(file))),\
      str(get_version_number(file))))
#create statistic file
key=list(Counter(exts).keys())
value=list(Counter(exts).values())
with open(path+"\\statistics.txt","w") as f:
   f.write("Statistics: " + path + "\n" +"\n")
   f.write("All files: " + str(len(lists))+"\n")
   for i in range (0,len(key)):
       f.write(" *"+str(key[i]) + " - " + str(value[i])+"\n")=="*"

#end script
print("Script ends")







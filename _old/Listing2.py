from tkinter import filedialog
from tkinter import *
import os
import datetime
import sys
from tkinter import ttk
from collections import Counter

class Listing:
    def __init__(self,master,st=0):
        self.master=master
        master.title('Listing')
        #progressbar
        self.pb = ttk.Progressbar(master,orient ="horizontal",length = 200, mode ="determinate")
        self.pb.grid(row=1,column=1)
        self.pb["maximum"] = 100   
        self.pb["value"]=st
        #menu
        self.label_1 = Label(master, text="Path: ")
        self.label_1.grid(row=0)
        self.entry_1=Entry(master, width=40)
        self.entry_1.insert(END,'')
        self.entry_1.grid(row=0,column=1)

        self.label_2 = Label(master, text="")
        self.label_2.grid(row=1, column=3)

        self.button_1 = Button(master, text="Get path", command=self.main, height=1,width=10)
        self.button_1.grid(row=0,column=3)

        self.close_button = Button(master, text="Exit", command=root.destroy, height=1,width=5)
        self.close_button.grid(row=3,column=4)
        
    def update(self, value):      
        self.pb["value"]=value
        self.master.update()
        self.label_2.config(text=(str(round(value))+" %"))

    def checker(self,value,maxx):
        check=round((value/maxx)*100,1)
        if (check % 1)==0:
            self.update(check)
        
    def main(self):
        #get path 
        window=Tk()
        window.withdraw()
        window.title("Listing")
        self.label_2['text']="loading files"
        path =  filedialog.askdirectory(parent=window,initialdir=os.getcwd(),title='Please select a directory')
        if path=="":
            self.label_2['text']="Select path!"
            return          
        self.entry_1.delete(0,END)
        self.entry_1.insert(0,path)
        lists=[]#files list
        exts=[] #extensions list
        exts_unique=[] #unique extensions list       
        #get files from path
        for root, dirs, files in os.walk(path):
           for name in files:
               lists.append(root+"\\"+name) #file list
               filename, file_extension = os.path.splitext(name)
               exts.append(file_extension)     
        files=len(lists) #count files go listing
        exts_unique=list(set(exts)) #get unique values from extensions list
        #save listing to file
        with open(path+"\\listing.txt","w") as f: 
           f.write("File with listing from folder: " + path + "\n" +"\n"+ "name ; path ; size [B] ; creation_time ; modification_time ; last_use_time\n")  
           i=0
           for file in lists:
              f.write("\n {} ; {} ; {} ; {} ; {} ; {}".format(str(file[file.rfind("\\",)+1:]), file, str(os.path.getsize(file)), str(datetime.datetime.fromtimestamp(os.path.getctime(file))),\
              str(datetime.datetime.fromtimestamp(os.path.getmtime(file))), str(datetime.datetime.fromtimestamp(os.path.getatime(file)))))
              i+=1
              bar.checker(i,files)    
        #create statistic file
        key=list(Counter(exts).keys())
        value=list(Counter(exts).values())
        with open(path+"\\statistics.txt","w") as f:
           f.write("Statistics: " + path + "\n" +"\n")
           f.write("All files: " + str(len(lists))+"\n")
           for i in range (0,len(key)):
               f.write(" *"+str(key[i]) + " - " + str(value[i])+"\n")=="*"
        self.label_2['text']="100% Completed"   
        #end script
        print("Script ends")

root=Tk()
bar=Listing(root)
root.mainloop()





